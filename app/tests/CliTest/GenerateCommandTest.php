<?php


namespace App\Tests\CliTest;


use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class GenerateCommandTest extends KernelTestCase
{

    /** @test */
    public function cliExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $fileName = tempnam(sys_get_temp_dir(), 'code') . rand(0, 10000);

        $command = $application->find('codes:generate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--file' => $fileName,
            '--numberOfCodes' => 1000,
            '--lengthOdCode' => 5,
        ]);

        $this->assertFileExists($fileName);
    }

}