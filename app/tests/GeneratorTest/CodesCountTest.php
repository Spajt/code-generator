<?php


namespace App\Tests\GeneratorTest;

use App\Service\GeneratorService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CodesCountTest extends KernelTestCase
{
    /** @test */
    public function assertCodesCount()
    {
        self::bootKernel();
        /** @var ContainerInterface $container */
        $container = self::$kernel->getContainer();

        /** @var GeneratorService $generatorService */
        $generatorService = $container->get(GeneratorService::class);
        $generatedCodes = $generatorService->generateCodes(10000, 5);
        $this->assertEquals(10000, count($generatedCodes));

    }

}