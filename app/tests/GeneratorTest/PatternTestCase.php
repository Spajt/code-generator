<?php


namespace App\Tests\GeneratorTest;

use App\Service\GeneratorService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PatternTestCase extends KernelTestCase
{
    /** @test */
    public function assertPattern()
    {
        self::bootKernel();
        /** @var ContainerInterface $container */
        $container = self::$kernel->getContainer();

        /** @var GeneratorService $generatorService */
        $generatorService = $container->get(GeneratorService::class);

        $this->assertRegExp('/^[A-Za-z0-9]*$/', $generatorService->getPattern());

    }

}