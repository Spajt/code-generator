<?php


namespace App\Tests\GeneratorTest;

use App\Service\GeneratorService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UniqueCodesTest extends KernelTestCase
{
    /** @test */
    public function assertUniqueCodes()
    {
        self::bootKernel();
        /** @var ContainerInterface $container */
        $container = self::$kernel->getContainer();

        /** @var GeneratorService $generatorService */
        $generatorService = $container->get(GeneratorService::class);
        $generatedCodes = $generatorService->generateCodes(10000, 5);
        $this->assertEquals(count(array_unique($generatedCodes)), count($generatedCodes));
    }

}