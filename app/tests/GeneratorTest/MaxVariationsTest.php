<?php


namespace App\Tests\GeneratorTest;

use App\Exceptions\MaxVariationsExceededException;
use App\Service\GeneratorService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MaxVariationsTest extends KernelTestCase
{
    /** @test */
    public function assertMaxVariations()
    {
        self::bootKernel();
        /** @var ContainerInterface $container */
        $container = self::$kernel->getContainer();

        /** @var GeneratorService $generatorService */
        $generatorService = $container->get(GeneratorService::class);
        $this->expectException(MaxVariationsExceededException::class);
        $generatorService->generateCodes(10000, 1);
    }

}