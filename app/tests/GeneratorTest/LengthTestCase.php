<?php


namespace App\Tests\GeneratorTest;

use App\Service\GeneratorService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LengthTestCase extends KernelTestCase
{
    /** @test */
    public function assertCodeLength()
    {
        self::bootKernel();
        /** @var ContainerInterface $container */
        $container = self::$kernel->getContainer();

        $generatorService = $container->get(GeneratorService::class);

        $this->assertEquals(5, strlen($generatorService->generate(5)));

    }

}