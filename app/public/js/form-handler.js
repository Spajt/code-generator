$(document).ready(function () {
    $(document).on('submit', 'form', function (e) {
        e.preventDefault();
        var form = $(this),
            formData = new FormData($(this)[0]),
            fileName = 'file';
        fetch(form.prop('action'), {
            method: 'POST',
            body: formData
        }).then(response => Promise.all([response, (!response.ok) ? response.json() : '']))
            .then(([resp, body]) => {
                console.log(resp.ok);
                if (!resp.ok) {
                    throw Error(body.message);
                }

                return resp
            })
            .then(resp => {
                fileName = resp.headers.get('Content-Disposition').replace('attachment; filename=', '');
                return resp;
            })
            .then(resp => resp.blob())
            .then(blob => {
                console.log(blob);
                const url = window.URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.href = url;
                // the filename you want
                a.download = fileName;
                document.body.appendChild(a);
                a.click();
                window.URL.revokeObjectURL(url);
            }).catch(resp => {
                alert(resp);
        });
    })

});
