<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

class RocketScienceException extends \Exception
{

    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = sprintf('Kosmiczne liczby wymagają kosmicznych rozwiązań');
        }

        parent::__construct($message, $code, $previous);
    }
}
