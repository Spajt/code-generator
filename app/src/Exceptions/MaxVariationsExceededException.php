<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

class MaxVariationsExceededException extends \Exception
{
    private $maxVariations;

    public function __construct($maxVariations = 0, $message = '', $code = 0, Throwable $previous = null)
    {
        $this->maxVariations = $maxVariations;
        if (empty($message)) {
            $message = sprintf('Maxiumum variations for this pattern and length is: %s', $maxVariations);
        }

        parent::__construct($message, $code, $previous);
    }

    public function getMaxVariations(): int
    {
        return $this->maxVariations;
    }
}
