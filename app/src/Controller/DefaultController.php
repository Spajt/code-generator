<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exceptions\MaxVariationsExceededException;
use App\Form\GeneratorType;
use App\Service\GeneratorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DefaultController extends AbstractController
{
    private $generatorService;

    private $validator;

    public function __construct(ValidatorInterface $validator, GeneratorService $generatorService)
    {
        $this->validator = $validator;
        $this->generatorService = $generatorService;
    }

    public function displayGeneratorForm(SessionInterface $session)
    {
        $data = null;

        $form = $this->createForm(GeneratorType::class, $data, [
            'action' => $this->generateUrl('generation_form'),
            'method' => 'POST',
        ]);

        if ($session->has('formData')) {
            $data = $session->get('formData');
            $session->clear();
            $form->submit($data);
        }

        return $this->render('base.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function generateCode(Request $request, SessionInterface $session)
    {
        $form = $this->createForm(GeneratorType::class, null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && !$form->isValid()) {
            $session->set('formData', $request->request->all());

            return new RedirectResponse('/');
        }
        $formData = $form->getData();
        $numberOfCodes = (int) $formData['numberOfCodes'];
        $lengthOfCodes = (int) $formData['lengthOfCodes'];

        try {
            $codes = $this->generatorService->generateCodes($numberOfCodes, $lengthOfCodes);
        } catch (MaxVariationsExceededException $exception) {
            return new JsonResponse([
                'message' => sprintf('Maksymalna ilość kodów tej długości to: %s', $exception->getMaxVariations())
            ], Response::HTTP_BAD_REQUEST);
        }

        $response = new Response(implode(\PHP_EOL, $codes));
        $filename = sprintf('Kody-ilosc-%s-o-dlugosci-%s.txt', $numberOfCodes, $lengthOfCodes);

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
