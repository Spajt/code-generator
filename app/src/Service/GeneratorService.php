<?php

declare(strict_types=1);

namespace App\Service;

use App\Exceptions\MaxVariationsExceededException;
use App\Exceptions\RocketScienceException;

class GeneratorService
{
    public function generateCodes(int $codesCount, int $codeLength = 5): array
    {
        $this->checkMaxVariations($codesCount, $codeLength);

        $codes = [];
        while (count($codes) < $codesCount) {
            $random = $this->generate($codeLength);

            if (!in_array($random, $codes)) {
                $codes[] = $random;
            }
        }

        return $codes;
    }

    public function generate($length = 5): string
    {
        $randomString = '';
        $pattern = $this->getPattern();
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $pattern[rand(0, strlen($pattern) - 1)];
        }

        return $randomString;
    }

    public function getPattern(): string
    {
        return '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    }

    protected function checkMaxVariations(int $codesCount, int $length): void
    {
        if ($codesCount >= PHP_INT_MAX) {
            throw new RocketScienceException();
        }

        $maxVariations = $this->countMaxVariations($length, $this->getPattern());
        if ($codesCount > $maxVariations) {
            throw new MaxVariationsExceededException($maxVariations);
        }
    }

    protected function countMaxVariations(int $length, string $pattern): int
    {
        $result = (strlen($pattern) ** $length);
        if (is_infinite($result)) {
            return PHP_INT_MAX - 1;
        }

        return (int) number_format($result, 0, '.', '');
    }
}
