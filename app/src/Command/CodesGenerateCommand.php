<?php

declare(strict_types=1);

namespace App\Command;

use App\Exceptions\MaxVariationsExceededException;
use App\Service\GeneratorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CodesGenerateCommand extends Command
{
    private $generatorService;

    protected static $defaultName = 'codes:generate';

    public function __construct(GeneratorService $generatorService, string $name = null)
    {
        $this->generatorService = $generatorService;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->addOption('file', 'f', InputOption::VALUE_REQUIRED, 'Path to file with codes')
            ->addOption('numberOfCodes', 'noc', InputOption::VALUE_REQUIRED, 'Number of generated codes')
            ->addOption('lengthOdCode', 'loc', InputOption::VALUE_REQUIRED, 'Length of single code')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $filePath = $input->getOption('file');
        $numberOfCodes = (int) $input->getOption('numberOfCodes');
        $lengthOdCode = (int) $input->getOption('lengthOdCode');

        try {
            $codes = $this->generatorService->generateCodes($numberOfCodes, $lengthOdCode);
        } catch (MaxVariationsExceededException $exception) {
            $io->error(sprintf('Maksymalna ilość kodów tej długości to: %s', $exception->getMaxVariations()));

            return Command::FAILURE;
        }

        file_put_contents($filePath, implode(\PHP_EOL, $codes));

        $io->success(sprintf('Plik został wygenerowany, znajdziesz go w: %s', $filePath));

        return Command::SUCCESS;
    }
}
