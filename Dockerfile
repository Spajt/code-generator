FROM php:7.4-apache
ARG ENV_TYPE
RUN apt update && apt install libzip-dev git zip -y


RUN if [ "$ENV_TYPE" = "dev"  ]; then \
       docker-php-ext-enable xdebug && \
       echo '#[xdebug]\n' >> /usr/local/etc/php/php.ini && \
       echo 'xdebug.remote_port=9000\n' >> /usr/local/etc/php/php.ini && \
       echo 'xdebug.remote_enable=1\n' >> /usr/local/etc/php/php.ini && \
       echo 'xdebug.remote_connect_back=1\n' >> /usr/local/etc/php/php.ini && \
       echo '\n' >> /usr/local/etc/php/php.ini \
   ; else  echo "Xdebug not installed" ; fi

RUN docker-php-ext-install zip
RUN docker-php-ext-enable zip
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN a2enmod rewrite

RUN cd /var/www && composer install